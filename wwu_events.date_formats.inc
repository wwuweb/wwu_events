<?php

function _wwu_events_date_format_names() {
  return array(
    'wwu_events_long_date_month',
    'wwu_events_long_day_month_date',
    'wwu_events_short_month_date_time',
    'wwu_events_twelve_hour_time',
  );
}

function _wwu_events_date_format_formats() {
  return array(
    'j F',
    'l, F j',
    'M j g:ia',
    'g:i A',
  );
}

function _wwu_events_date_format_types() {
  return array(
    t('Date Month (Long)'),
    t('Day (Long), Month (Long) Date'),
    t('Month (Short) Date Time'),
    t('Twelve-Hour Time'),
  );
}
