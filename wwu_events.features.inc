<?php
/**
 * @file
 * wwu_events.features.inc
 */

/**
 * Implements hook_views_api().
 */
function wwu_events_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function wwu_events_node_info() {
  $items = array(
    'wwu_event' => array(
      'name' => t('WWU Event'),
      'base' => 'node_content',
      'description' => t('Use the <em>WWU Event</em> content type to post events to college or department calendars.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
