<?php
/**
 * @file
 * wwu_events.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function wwu_events_taxonomy_default_vocabularies() {
  return array(
    'wwu_event_calendars' => array(
      'name' => 'Event Calendars',
      'machine_name' => 'wwu_event_calendars',
      'description' => 'The calendar(s) to which an event is assigned and displayed.',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
    'wwu_event_locations' => array(
      'name' => 'Event Locations',
      'machine_name' => 'wwu_event_locations',
      'description' => 'Locations reference for the Event content type.',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
  );
}
